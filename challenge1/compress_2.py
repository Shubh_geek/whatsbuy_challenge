# Online Python compiler (interpreter) to run Python online.
# Write Python 3 code in this online editor and run it.


def compress(input_string):
    """
    Takes in a string input_string, returns the compressed version of it
    Parameters
    ----------
    input_string: List[char]

    Returns
    -------
    result: str
    """
    # Check for None or empty string
    if input_string is None:
        return result
    length = len(input_string)
    # Write pointer to keep track of the index where we have to overwrite.
    write = 0
    # Initalize the Counter
    count = 1
    for index, char in enumerate(input_string):
        """
        We should check if we are at the last index or next index value
        differs from the current index value. In Either case, we have to do
        the overwritting with its count.
        """
        if index+1 == length or char != input_string[index+1]:
            input_string[write] = input_string[index]
            write = write + 1
            """
            Since, we have taken list as input, we will write the digit at each
            index.
            """
            if count > 1:
                for x in str(count):
                    input_string[write] = x
                    write = write+1
            # Re-initialize the next blocks' counter
            count = 1
        else:
            # otherwise, increase the counter.
            count = count+1
    # construct the string till the last over written index and return it
    result = "".join(input_string[:write])
    return result

if __name__ == '__main__':
    raw_string = list(input("Enter a string to compress: "))
    result = compress(raw_string)
    print(result)

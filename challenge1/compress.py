# Online Python compiler (interpreter) to run Python online.
# Write Python 3 code in this online editor and run it.


def compress(input_string):
    """
    Takes in a string input_string, returns the compressed version of it

    Parameters
    ----------
    input_string: str

    Returns
    -------
    result: str
    """

    result = ""
    # Check for None or empty string
    if input_string is None or input_string == "":
        return result
    len_of_string = len(input_string)
    # initializing the first block
    prev = input_string[0]
    # Initializing the first block's counter
    count = 1
    # Starting the scan on the input string from the 1st index
    for x in range(1, len_of_string):
        """
        If the current char continues to matche with the starting of the
        block, we shall just increment the count.
        """
        if input_string[x] == prev:
            count = count + 1
        else:
            """
            once the block of continuous char ends, we shall append the char
            and its count on to result string and start the new block
            """
            result = result + prev
            """
            Adding the count only when it's greater than 1 to avoid increasing
            the length of the compressed string.
            """
            if count > 1:
                result = result + "{}".format(count)
            # Initialize the new block.
            prev = input_string[x]
            # Re-initialize the count for the next block
            count = 1
    # return the final result
    return result

if __name__ == '__main__':
    raw_string = input("Enter a string to compress: ")
    result = compress(raw_string)
    print(result)

# Online Python compiler (interpreter) to run Python online.
# Write Python 3 code in this online editor and run it.


from collections import defaultdict


class Graph(object):
    """
    A class to represent a Graph.

    ...

    Attributes
    ----------
    graph : dict[list]
        to store the node to node mappings
    degree : dict[int]
        to store count of each node's both in and out degrees

    Methods
    -------
    build_graph(edges=[]):
        builds the adjancy graph and counts the degree in parallel.
    add_edge(edge=[]):
        adds a single edge to graph and updates the degree.
    identify_router():
        returns the list of nodes having most number of connections
    """
    def __init__(self):
        """
        Constructor - Initializes the object

        """
        self.graph = defaultdict(list)
        self.degree = defaultdict(int)

    def build_graph(self, edges=[]):
        """
        Builds the adjancy graph and counts the degree in parallel.

        Parameters
        ----------
        edges : List[List]

        Returns
        -------
        None
        """
        for u, v in edges:
            self.graph[u].append(v)
            self.degree[u] += 1
            self.degree[v] += 1

    def add_edge(self, edge=[]):
        """
        Adds a single edge to graph and updates the degree.

        Parameters
        ----------
        edges : List[]

        Returns
        -------
        None
        """
        u, v = edge
        self.graph[u].append(v)
        self.degree[u] += 1
        self.degree[v] += 1

    def identify_router(self):
        """
        Returns the list of nodes having most number of connections

        Parameters
        ----------
        None

        Returns
        -------
        List[]
        """
        # Find the max value among all degrees
        max_degree = max(self.degree.items(), key=lambda x: x[1])
        list_of_labels = list()
        # Iterate over all the items in dictionary to find keys with max value
        print(max_degree)
        for key, value in self.degree.items():
            if value == max_degree[1]:
                list_of_labels.append(key)
        return list_of_labels

if __name__ == '__main__':
    # edges - [['1', '2'], ['2', '3']]
    edges = []
    n = int(input("Enter number of edges : "))
    for i in range(0, n):
        ele = [input(), input()]
        edges.append(ele)
    graph = Graph()
    graph.build_graph(edges)
    hot_nodes = graph.identify_router()
    print(hot_nodes)
